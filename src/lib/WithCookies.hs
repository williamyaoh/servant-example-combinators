{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

-- | Servant combinator that parses the @Cookies:@ header and passes the parsed
-- | cookies to the handler, and nothing else. For a more involved cookie
-- | combinator that can do things like provide cookies to other combinators,
-- | or avoid parsing cookies multiple times, see the "ProvideCookies" module.

module WithCookies
  ( WithCookies
  , TestAPI, testApplication
  )
where

import Data.Bifunctor ( bimap )
import Data.ByteString ( ByteString )
import Data.Map.Strict ( Map )
import Data.Text ( Text )
import Data.Text.Encoding ( decodeUtf8 )
import Network.HTTP.Types.Header ( hCookie )
import Network.Wai ( requestHeaders )
import Servant
  ( HasServer(..), Server, Handler
  , Get, NoContent(..)
  , Headers, Header
  , JSON
  , (:>), (:<|>)(..)
  , Application
  , Proxy(..)
  , addHeader
  , serve
  )
import Servant.Server.Internal.Delayed ( passToServer )
import Web.Cookie ( SetCookie, defaultSetCookie, setCookieName, setCookieValue, parseCookies )

import qualified Data.Map.Strict as Map

type Cookies = Map ByteString ByteString

data WithCookies

instance HasServer api ctx => HasServer (WithCookies :> api) ctx where
  type ServerT (WithCookies :> api) m = Cookies -> ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt . server

  route _ ctx server = route (Proxy @api) ctx $
    server `passToServer` \req ->
      let mcookie = lookup hCookie (requestHeaders req)
      in maybe Map.empty (Map.fromList . parseCookies) mcookie

type TestAPI =
       "add-cookie" :>
         Get '[JSON] (Headers '[Header "Set-Cookie" SetCookie] NoContent)
  :<|> "show-cookies" :>
         WithCookies :> Get '[JSON] (Map Text Text)

testServer :: Server TestAPI
testServer = addCookie :<|> showCookies
  where
    addCookie :: Handler (Headers '[Header "Set-Cookie" SetCookie] NoContent)
    addCookie =
      let cookie = defaultSetCookie
            { setCookieName = "TEST_COOKIE"
            , setCookieValue = "FOOBAR"
            }
      in pure $ addHeader cookie NoContent

    showCookies :: Cookies -> Handler (Map Text Text)
    showCookies cookies = do
      let kvs = Map.toList cookies
      pure $ Map.fromList $ fmap (bimap decodeUtf8 decodeUtf8) kvs

testApplication :: Application
testApplication = serve (Proxy @TestAPI) testServer
