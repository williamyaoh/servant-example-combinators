{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE KindSignatures            #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}

-- | Servant combinators that check the @Content-Length@ header on incoming
-- | requests, and returns an HTTP 413 if it exceeds a predetermined limit.

module RequestSizeLimit
  ( WithSizeCheck, RequestSizeLimit, StorageUnit(..)
  , SizeLimitKey(..)
  , TestAPI, mkTestApplication
  )
where

import Control.Monad ( when )
import Data.Functor ( (<&>) )
import GHC.TypeLits ( KnownNat, Natural, natVal )
import Network.Wai ( RequestBodyLength(..), requestBodyLength, vault )
import Servant
  ( HasServer(..), Server
  , NoContent(..), GetNoContent
  , (:>), (:<|>)(..)
  , Context(..), HasContextEntry(..)
  , Application
  , Proxy(..)
  , serveWithContext
  , err413
  )
import Servant.Server.Internal.Delayed ( addHeaderCheck )
import Servant.Server.Internal.DelayedIO ( withRequest, delayedFailFatal )

import qualified Data.Vault.Lazy as Vault

data StorageUnit = B | KB | MB | GB

-- | Checks that the incoming request's @Content-Length@ header is within the
-- | limit set by a `RequestSizeLimit' combinator. If multiple `RequestSizeLimit'
-- | combinators are on the given path, the limit from the innermost combinator
-- | is used. If /no/ size limits are set, no check is performed.
data WithSizeCheck

data RequestSizeLimit (amt :: Natural) (unit :: StorageUnit)

newtype SizeLimitKey = SizeLimitKey { getSizeLimitKey :: Vault.Key Int }

class KnownStorageUnit (unit :: StorageUnit) where
  storageUnitVal :: Int

instance KnownStorageUnit 'B where
  storageUnitVal = 1
instance KnownStorageUnit 'KB where
  storageUnitVal = 1024
instance KnownStorageUnit 'MB where
  storageUnitVal = 1024 * 1024
instance KnownStorageUnit 'GB where
  storageUnitVal = 1024 * 1024 * 1024

instance
  ( HasServer api ctx
  , HasContextEntry ctx SizeLimitKey
  )
  => HasServer (WithSizeCheck :> api) ctx where
  type ServerT (WithSizeCheck :> api) m = ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt server

  route _ ctx server =
    let serverDummyArg = fmap const server
    in route (Proxy @api) ctx $
         addHeaderCheck
           serverDummyArg
           (withRequest $ \req -> do
             let
               SizeLimitKey key = getContextEntry ctx
               mlimit = Vault.lookup key (vault req)
             case (mlimit, requestBodyLength req) of
               (Nothing, _) -> pure ()
               (_, ChunkedBody) -> pure ()
               (Just limit, KnownLength len) ->
                 when (len > fromIntegral limit) $
                   delayedFailFatal err413)

instance
  ( HasServer api ctx
  , HasContextEntry ctx SizeLimitKey
  , KnownNat amt
  , KnownStorageUnit unit
  )
  => HasServer (RequestSizeLimit amt unit :> api) ctx where
  type ServerT (RequestSizeLimit amt unit :> api) m = ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt server

  route _ ctx server =
    route (Proxy @api) ctx server <&> \app req respK -> do
      let
        limit = fromIntegral (natVal (Proxy @amt)) * storageUnitVal @unit
        SizeLimitKey key = getContextEntry ctx
        req' = req { vault = Vault.insert key limit (vault req) }
      app req' respK

type TestAPI =
  WithSizeCheck :> RequestSizeLimit 2 'KB :>
       ( "normal-limit" :> GetNoContent
    :<|> "small-limit" :> RequestSizeLimit 16 'B :> GetNoContent
    :<|> "large-limit" :> RequestSizeLimit 16 'KB :> GetNoContent
       )

testServer :: Server TestAPI
testServer = pure NoContent :<|> pure NoContent :<|> pure NoContent

mkTestApplication :: IO Application
mkTestApplication = do
  key <- Vault.newKey <&> SizeLimitKey
  pure $ serveWithContext (Proxy @TestAPI) (key :. EmptyContext) testServer
