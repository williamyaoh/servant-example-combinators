{-# OPTIONS -Wno-redundant-constraints #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}

-- | Servant combinators that parse cookies from the current request and provide
-- | them to other Servant combinators in the same stack. Useful if you need
-- | lots of independent combinators that deal with cookies, and don't want
-- | each one to reparse.

module ProvideCookies
  ( ProvideCookies
  , WithCookies
  , CheckTestCookie
  , Cookies
  , HasCookies(..)
  , mkTestApplication
  )
where

import Data.Bifunctor ( bimap )
import Data.ByteString ( ByteString )
import Data.Functor ( (<&>) )
import Data.Map.Strict ( Map )
import Data.Text ( Text )
import Data.Text.Encoding ( decodeUtf8 )
import Network.HTTP.Types.Header ( hCookie )
import Network.Wai ( requestHeaders, vault )
import Servant
  ( HasServer(..), Server, Handler
  , Get, NoContent(..)
  , Headers, Header
  , JSON
  , (:>), (:<|>)(..)
  , Context(..), HasContextEntry(..)
  , ServerError(..)
  , Application
  , Proxy(..)
  , addHeader
  , serveWithContext
  , err400, err500
  )
import Servant.Server.Internal.Delayed ( addAcceptCheck, addHeaderCheck )
import Servant.Server.Internal.DelayedIO ( DelayedIO, withRequest, delayedFailFatal )
import Web.Cookie ( SetCookie, defaultSetCookie, setCookieName, setCookieValue, parseCookies )

import qualified Data.Map.Strict as Map
import qualified Data.Vault.Lazy as Vault

type Cookies = Map ByteString ByteString

-- | Combinator that actually handles parsing the cookies from the current
-- | request, and caches them for other combinators to use. Put this at the
-- | top of your Servant API so that other combinators can access cookies.
data ProvideCookies

-- | Passes cookies to Servant handlers, by retrieving the cookies cached by
-- | `ProvideCookies'.
data WithCookies

data HasCookies = HasCookies

instance
  ( HasServer api (HasCookies ': ctx)
  , HasContextEntry ctx (Vault.Key Cookies)
  )
  => HasServer (ProvideCookies :> api) ctx where
  type ServerT (ProvideCookies :> api) m = ServerT api m

  hoistServerWithContext _ _ nt server =
    hoistServerWithContext (Proxy @api) (Proxy @(HasCookies ': ctx)) nt server

  route _ ctx server =
    route (Proxy @api) (HasCookies :. ctx) server <&> \app req respK -> do
      let
        mcookie = lookup hCookie (requestHeaders req)
        cookies = maybe Map.empty (Map.fromList . parseCookies) mcookie
        key = getContextEntry ctx :: Vault.Key Cookies
        req' = req { vault = Vault.insert key cookies (vault req) }
      app req' respK

instance
  ( HasServer api ctx
  , HasContextEntry ctx HasCookies
  , HasContextEntry ctx (Vault.Key Cookies)
  )
  => HasServer (WithCookies :> api) ctx where
  type ServerT (WithCookies :> api) m = Cookies -> ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt . server

  route _ ctx server =
    route (Proxy @api) ctx $
      server `addHeaderCheck` retrieveCookies
    where
      retrieveCookies :: DelayedIO Cookies
      retrieveCookies = withRequest $ \req -> do
        let key = getContextEntry ctx :: Vault.Key Cookies
        case Vault.lookup key (vault req) of
          Just cookies -> pure cookies
          Nothing -> delayedFailFatal $ err500
            { errBody = "Something has gone horribly wrong; could not find cached cookies." }

-- | The value of splitting the parsing and the retrieval becomes more obvious
-- | if we have multiple combinators that need to use the request cookies, since
-- | we can avoid reparsing. Here, we just check that the @TEST_COOKIE@ cookie is
-- | set and send back a 400 if it's not.
data CheckTestCookie

instance
  ( HasServer api ctx
  , HasContextEntry ctx HasCookies
  , HasContextEntry ctx (Vault.Key Cookies)
  )
  => HasServer (CheckTestCookie :> api) ctx where
  type ServerT (CheckTestCookie :> api) m = ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt server

  route _ ctx server =
    route (Proxy @api) ctx $
      server `addAcceptCheck` checkTestCookie
    where
      checkTestCookie :: DelayedIO ()
      checkTestCookie = withRequest $ \req -> do
        let key = getContextEntry ctx :: Vault.Key Cookies
        case Vault.lookup key (vault req) >>= Map.lookup "TEST_COOKIE" of
          Just _ -> pure ()
          Nothing -> delayedFailFatal $ err400
            { errBody = "TEST_COOKIE cookie not set." }

type TestAPI = ProvideCookies :>
     ( "add-cookie" :>
         Get '[JSON] (Headers '[Header "Set-Cookie" SetCookie] NoContent)
  :<|> "check-cookies" :>
         CheckTestCookie :> WithCookies :> Get '[JSON] (Map Text Text)
     )

testServer :: Server TestAPI
testServer = addCookie :<|> showCookies
  where
    addCookie :: Handler (Headers '[Header "Set-Cookie" SetCookie] NoContent)
    addCookie =
      let cookie = defaultSetCookie
            { setCookieName = "TEST_COOKIE"
            , setCookieValue = "FOOBAR"
            }
      in pure $ addHeader cookie NoContent

    showCookies :: Cookies -> Handler (Map Text Text)
    showCookies cookies = do
      let kvs = Map.toList cookies
      pure $ Map.fromList $ fmap (bimap decodeUtf8 decodeUtf8) kvs

mkTestApplication :: IO Application
mkTestApplication = do
  key <- Vault.newKey :: IO (Vault.Key Cookies)
  pure $ serveWithContext
    (Proxy @TestAPI)
    (key :. EmptyContext)
    testServer
