{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

-- | Servant combinator that prints "Hello, Servant!" to @stdout@ on every
-- | request underneath the combinator.

module HelloServant
  ( HelloServant
  , TestAPI, testApplication
  )
where

import Data.Functor ( (<&>) )
import Servant
  ( HasServer(..), Server
  , GetNoContent, NoContent(..)
  , (:>), (:<|>)(..)
  , Application
  , Proxy(..)
  , serve
  )

data HelloServant

instance HasServer api ctx => HasServer (HelloServant :> api) ctx where
  type ServerT (HelloServant :> api) m = ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt server

  route _ ctx server = route (Proxy @api) ctx server <&> \app req respK -> do
    putStrLn "Hello, Servant!"
    app req respK

type TestAPI =
       "nolog" :> GetNoContent
  :<|> "log" :> HelloServant :> GetNoContent

testServer :: Server TestAPI
testServer = pure NoContent :<|> pure NoContent

testApplication :: Application
testApplication = serve (Proxy @TestAPI) testServer
