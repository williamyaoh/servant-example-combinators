{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}

-- | Servant combinator that sends back the full path (including query
-- | parameters) that the client used to make the request.

module Replay
  ( Replay
  , TestAPI, testApplication
  )
where

import Data.Functor ( (<&>) )
import Network.Wai ( rawPathInfo, rawQueryString, mapResponseHeaders )
import Servant
  ( HasServer(..), Server
  , GetNoContent, NoContent(..)
  , (:>), (:<|>)(..)
  , Application
  , Proxy(..)
  , serve
  )

data Replay

instance HasServer api ctx => HasServer (Replay :> api) ctx where
  type ServerT (Replay :> api) m = ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt server

  route _ ctx server = route (Proxy @api) ctx server <&> \app req respK -> do
    let replayPath = rawPathInfo req <> rawQueryString req
    let respK' = respK . fmap (mapResponseHeaders (("X-Replay-Path", replayPath) :))
    app req respK'

type TestAPI =
       "noreplay" :> GetNoContent
  :<|> "replay" :> Replay :> GetNoContent

testServer :: Server TestAPI
testServer = pure NoContent :<|> pure NoContent

testApplication :: Application
testApplication = serve (Proxy @TestAPI) testServer
