{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

-- | Servant combinator that passes the raw request to the handler.

module WithRawRequest
  ( WithRawRequest
  , TestAPI, testApplication
  )
where

import Control.Monad.IO.Class ( liftIO )
import Network.Wai ( Request )
import Servant
  ( HasServer(..), Server
  , GetNoContent, NoContent(..)
  , (:>)
  , Application
  , Proxy(..)
  , serve
  )
import Servant.Server.Internal.Delayed ( passToServer )

data WithRawRequest

instance HasServer api ctx => HasServer (WithRawRequest :> api) ctx where
  type ServerT (WithRawRequest :> api) m = Request -> ServerT api m

  hoistServerWithContext _ ctx nt server =
    hoistServerWithContext (Proxy @api) ctx nt . server

  route _ ctx server = route (Proxy @api) ctx $
    passToServer server id

type TestAPI =
  WithRawRequest :> GetNoContent

testServer :: Server TestAPI
testServer req = do
  liftIO $ print req
  pure NoContent

testApplication :: Application
testApplication = serve (Proxy @TestAPI) testServer
