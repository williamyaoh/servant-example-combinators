{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists       #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE QuasiQuotes           #-}

module WithCookiesTest
  ( spec )
where

import Control.Exception ( throwIO )
import Data.Foldable ( toList )
import Data.Map.Strict ( Map )
import Data.String.Interpolate ( i )
import Data.Text ( Text )
import Network.HTTP.Client ( newManager, defaultManagerSettings )
import Network.HTTP.Types.Header ( hSetCookie )
import Network.Wai.Handler.Warp ( testWithApplication )
import Servant ( Proxy(..), Get, Header', Required, Strict, Raw, JSON, (:>), (:<|>)(..) )
import Servant.Client ( HasClient(..), ClientEnv, ClientM, parseBaseUrl, mkClientEnv, runClientM, client )
import Servant.Client.Core.Response ( ResponseF(responseHeaders) )
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, assertEqual )
import WithCookies ( testApplication )

import qualified Network.HTTP.Types as HTTP

type TestAPI' =
       "add-cookie" :> Raw
  :<|> "show-cookies" :> Header' '[Required, Strict] "Cookie" Text :> Get '[JSON] (Map Text Text)

spec :: TestTree
spec = testGroup "WithCookies"
  [ testCase "add-cookie" $
      withServer $ \cl clEnv -> do
        let addCookie :<|> _showCookie = cl
        response <- runClientM (addCookie HTTP.methodGet) clEnv >>= either throwIO pure
        let mcookie = lookup hSetCookie $ toList $ responseHeaders response
        assertEqual "got back expected cookie header" (Just "TEST_COOKIE=FOOBAR") mcookie

  , testCase "show-cookies" $ do
      withServer $ \cl clEnv -> do
        let _addCookie :<|> showCookie = cl
        cookies <- runClientM (showCookie "COOKIE1=A;COOKIE2=B") clEnv >>= either throwIO pure
        assertEqual "got back expected cookies"
          [ ("COOKIE1", "A"), ("COOKIE2", "B") ]
          cookies
  ]

withServer :: (Client ClientM TestAPI' -> ClientEnv -> IO ()) -> IO ()
withServer body =
  testWithApplication (pure testApplication) $ \port -> do
    manager <- newManager defaultManagerSettings
    baseURL <- parseBaseUrl [i|localhost:#{port}|]
    let clientEnv = mkClientEnv manager baseURL
    let cl = client (Proxy @TestAPI')
    body cl clientEnv
