{-# OPTIONS -Wno-orphans           #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE QuasiQuotes           #-}

module WithRawRequestTest
  ( spec )
where

import Control.Exception ( throwIO, bracket )
import Data.String.Interpolate ( i )
import Network.HTTP.Client ( newManager, defaultManagerSettings )
import Network.Wai.Handler.Warp ( testWithApplication )
import Servant ( Proxy(..), (:>) )
import Servant.Client ( HasClient(..), ClientEnv, ClientM, parseBaseUrl, mkClientEnv, runClientM, client )
import System.Directory ( removeFile )
import System.Posix ( Fd, OpenMode(..), defaultFileFlags, stdOutput, openFd, fdRead, closeFd, dup, dupTo )
import System.IO ( hClose, openTempFile )
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, assertEqual )
import WithRawRequest ( WithRawRequest, TestAPI, testApplication )

instance HasClient m api => HasClient m (WithRawRequest :> api) where
  type Client m (WithRawRequest :> api) = Client m api

  clientWithRoute m _ req =
    clientWithRoute m (Proxy @api) req

  hoistClientMonad m _ nt cl =
    hoistClientMonad m (Proxy @api) nt cl

spec :: TestTree
spec = testGroup "WithRawRequest"
  [ testCase "endpoint that logs raw request" $
      withServer $ \cl clEnv ->
        withStdoutRedirect $ \fd -> do
          _ <- runClientM cl clEnv >>= either throwIO pure
          (logResult, _) <- fdRead fd 9999
          assertEqual "endpoint logs raw request" "Request {" (take 9 logResult)
  ]

withServer :: (Client ClientM TestAPI -> ClientEnv -> IO ()) -> IO ()
withServer body =
  testWithApplication (pure testApplication) $ \port -> do
    manager <- newManager defaultManagerSettings
    baseURL <- parseBaseUrl [i|localhost:#{port}|]
    let clientEnv = mkClientEnv manager baseURL
    let cl = client (Proxy @TestAPI)
    body cl clientEnv

withStdoutRedirect :: (Fd -> IO ()) -> IO ()
withStdoutRedirect body =
  bracket
    (do (fp, writeHdl) <- openTempFile "." "servant-example-combinator-testXXXXXX.txt"
        oldStdout <- dup stdOutput
        hClose writeHdl
        writeFd <- openFd fp WriteOnly Nothing defaultFileFlags
        readFd <- openFd fp ReadOnly Nothing defaultFileFlags
        _ <- dupTo writeFd stdOutput
        pure (oldStdout, writeFd, readFd, fp))
    (\(oldStdout, writeFd, readFd, fp) -> do
      closeFd writeFd
      closeFd readFd
      _ <- dupTo oldStdout stdOutput
      removeFile fp)
    (\(_, _, fd, _) -> body fd)
