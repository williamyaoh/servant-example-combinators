{-# OPTIONS -Wno-orphans           #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE QuasiQuotes           #-}

module HelloServantTest
  ( spec )
where

import Prelude hiding ( log )

import Control.Exception ( IOException, throwIO, try, bracket )
import Control.Monad ( void )
import Data.String.Interpolate ( i )
import HelloServant ( HelloServant, TestAPI, testApplication )
import Network.HTTP.Client ( newManager, defaultManagerSettings )
import Network.Wai.Handler.Warp ( testWithApplication )
import Servant ( Proxy(..), (:>), (:<|>)(..) )
import Servant.Client ( HasClient(..), ClientEnv, ClientM, parseBaseUrl, mkClientEnv, runClientM, client )
import System.Directory ( removeFile )
import System.Posix ( Fd, OpenMode(..), defaultFileFlags, stdOutput, openFd, fdRead, closeFd, dup, dupTo )
import System.IO ( hClose, openTempFile )
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, assertBool, assertEqual )

instance HasClient m api => HasClient m (HelloServant :> api) where
  type Client m (HelloServant :> api) = Client m api

  clientWithRoute m _ req =
    clientWithRoute m (Proxy @api) req

  hoistClientMonad m _ nt cl =
    hoistClientMonad m (Proxy @api) nt cl

spec :: TestTree
spec = testGroup "HelloServant"
  [ testCase "logging endpoint" $ do
      withServer $ \cl clEnv ->
        withStdoutRedirect $ \fd -> do
          let (_nolog :<|> log) = cl
          _ <- runClientM log clEnv >>= either throwIO pure
          (logResult, _) <- fdRead fd 9999
          assertEqual "the logged value is as expected" "Hello, Servant!\n" logResult

  , testCase "non-logging endpoint" $ do
      withServer $ \cl clEnv ->
        withStdoutRedirect $ \fd -> do
          let (nolog :<|> _log) = cl
          _ <- runClientM nolog clEnv >>= either throwIO pure
          try (void $ fdRead fd 9999) >>= \case
            Left (_e :: IOException) -> pure ()
            Right s -> assertEqual "a value was logged, something went wrong" mempty s
  ]

withServer :: (Client ClientM TestAPI -> ClientEnv -> IO ()) -> IO ()
withServer body =
  testWithApplication (pure testApplication) $ \port -> do
    manager <- newManager defaultManagerSettings
    baseURL <- parseBaseUrl [i|localhost:#{port}|]
    let clientEnv = mkClientEnv manager baseURL
    let cl = client (Proxy @TestAPI)
    body cl clientEnv

withStdoutRedirect :: (Fd -> IO ()) -> IO ()
withStdoutRedirect body =
  bracket
    (do (fp, writeHdl) <- openTempFile "." "servant-example-combinator-testXXXXXX.txt"
        oldStdout <- dup stdOutput
        hClose writeHdl
        writeFd <- openFd fp WriteOnly Nothing defaultFileFlags
        readFd <- openFd fp ReadOnly Nothing defaultFileFlags
        _ <- dupTo writeFd stdOutput
        pure (oldStdout, writeFd, readFd, fp))
    (\(oldStdout, writeFd, readFd, fp) -> do
      closeFd writeFd
      closeFd readFd
      _ <- dupTo oldStdout stdOutput
      removeFile fp)
    (\(_, _, fd, _) -> body fd)
