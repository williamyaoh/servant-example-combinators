{-# OPTIONS -Wno-orphans           #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE QuasiQuotes           #-}

module RequestSizeLimitTest
  ( spec )
where

import Data.String.Interpolate ( i )
import Network.HTTP.Client ( newManager, defaultManagerSettings )
import Network.Wai.Handler.Warp ( testWithApplication )
import Servant ( Proxy(..), Header', Required, Strict, (:>), (:<|>)(..) )
import Servant.Client ( HasClient(..), ClientError(..), ClientEnv, ClientM, parseBaseUrl, mkClientEnv, runClientM, client )
import Servant.Client.Core.Response ( ResponseF(responseStatusCode) )
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, assertFailure )
import RequestSizeLimit ( WithSizeCheck, RequestSizeLimit, TestAPI, mkTestApplication )

import qualified Network.HTTP.Types as HTTP

instance HasClient m api => HasClient m (WithSizeCheck :> api) where
  type Client m (WithSizeCheck :> api) = Client m api

  clientWithRoute m _ req =
    clientWithRoute m (Proxy @api) req

  hoistClientMonad m _ nt cl =
    hoistClientMonad m (Proxy @api) nt cl

instance HasClient m api => HasClient m (RequestSizeLimit amt unit :> api) where
  type Client m (RequestSizeLimit amt unit :> api) = Client m api

  clientWithRoute m _ req =
    clientWithRoute m (Proxy @api) req

  hoistClientMonad m _ nt cl =
    hoistClientMonad m (Proxy @api) nt cl

spec :: TestTree
spec = testGroup "RequestSizeLimit"
  [ testCase "normal-limit and within limit" $
      withServer $ \cl clEnv -> do
        let normallimit :<|> _smalllimit :<|> _largelimit = cl 1024
        mresult <- runClientM normallimit clEnv
        case mresult of
          Left _ -> assertFailure "normal-limit rejected request within 2KB limit"
          Right _ -> pure ()

  , testCase "normal-limit and exceeds limit" $
      withServer $ \cl clEnv -> do
        let normallimit :<|> _smalllimit :<|> _largelimit = cl 4096
        mresult <- runClientM normallimit clEnv
        case mresult of
          Left (FailureResponse _ resp) | responseStatusCode resp == HTTP.status413 -> pure ()
          Right _ -> assertFailure "normal-limit didn't reject request exceeding 2KB limit"
          Left _ -> assertFailure "got back HTTP error other than HTTP 413"

  , testCase "small-limit and within limit" $
      withServer $ \cl clEnv -> do
        let _normallimit :<|> smalllimit :<|> _largelimit = cl 8
        mresult <- runClientM smalllimit clEnv
        case mresult of
          Left _ -> assertFailure "small-limit rejected request within 16B limit"
          Right _ -> pure ()

  , testCase "small-limit and exceeds limit" $
      withServer $ \cl clEnv -> do
        let _normallimit :<|> smalllimit :<|> _largelimit = cl 32
        mresult <- runClientM smalllimit clEnv
        case mresult of
          Left (FailureResponse _ resp) | responseStatusCode resp == HTTP.status413 -> pure ()
          Right _ -> assertFailure "small-limit didn't reject request exceeding 16B limit"
          Left _ -> assertFailure "got back HTTP error other than HTTP 413"

  , testCase "large-limit and within limit" $
      withServer $ \cl clEnv -> do
        let _normallimit :<|> _smalllimit :<|> largelimit = cl 8192
        mresult <- runClientM largelimit clEnv
        case mresult of
          Left _ -> assertFailure "large-limit rejected request within 16KB limit"
          Right _ -> pure ()

  , testCase "large-limit and exceeds limit" $
      withServer $ \cl clEnv -> do
        let _normallimit :<|> _smalllimit :<|> largelimit = cl 32768
        mresult <- runClientM largelimit clEnv
        case mresult of
          Left (FailureResponse _ resp) | responseStatusCode resp == HTTP.status413 -> pure ()
          Right _ -> assertFailure "large-limit didn't reject request exceeding 16KB limit"
          Left _ -> assertFailure "got back HTTP error other than HTTP 413"
  ]

withServer :: (Client ClientM (Header' '[Required, Strict] "Content-Length" Int :> TestAPI) -> ClientEnv -> IO ()) -> IO ()
withServer body = do
  testApplication <- mkTestApplication
  testWithApplication (pure testApplication) $ \port -> do
    manager <- newManager defaultManagerSettings
    baseURL <- parseBaseUrl [i|localhost:#{port}|]
    let clientEnv = mkClientEnv manager baseURL
    let cl = client (Proxy @(Header' '[Required, Strict] "Content-Length" Int :> TestAPI))
    body cl clientEnv
