{-# OPTIONS -Wno-orphans           #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE QuasiQuotes           #-}

module ReplayTest
  ( spec )
where

import Control.Exception ( throwIO )
import Data.Foldable ( toList )
import Data.String.Interpolate ( i )
import Network.HTTP.Client ( newManager, defaultManagerSettings )
import Network.Wai.Handler.Warp ( testWithApplication )
import Replay ( Replay, testApplication )
import Servant ( Proxy(..), Raw, QueryParam, (:>), (:<|>)(..) )
import Servant.Client ( HasClient(..), ClientEnv, ClientM, parseBaseUrl, mkClientEnv, runClientM, client )
import Servant.Client.Core.Response ( ResponseF(responseHeaders) )
import Test.Tasty ( TestTree, testGroup )
import Test.Tasty.HUnit ( testCase, assertEqual )

import qualified Network.HTTP.Types as HTTP

type TestAPI' =
       "noreplay" :> Raw
  :<|> "replay" :> Replay :> QueryParam "foo" String :> Raw

instance HasClient m api => HasClient m (Replay :> api) where
  type Client m (Replay :> api) = Client m api

  clientWithRoute m _ req =
    clientWithRoute m (Proxy @api) req

  hoistClientMonad m _ nt cl =
    hoistClientMonad m (Proxy @api) nt cl

spec :: TestTree
spec = testGroup "Replay"
  [ testCase "replay endpoint" $
      withServer $ \cl clEnv -> do
        let _ :<|> replay = cl
        response <- runClientM (replay (Just "bar") HTTP.methodGet) clEnv >>= either throwIO pure
        let mreplay = lookup "X-Replay-Path" $ toList $ responseHeaders response
        assertEqual "got back expected replay path" (Just "/replay?foo=bar") mreplay

  , testCase "no-replay endpoint" $
      withServer $ \cl clEnv -> do
        let noreplay :<|> _ = cl
        response <- runClientM (noreplay HTTP.methodGet) clEnv >>= either throwIO pure
        let mreplay = lookup "X-Replay-Path" $ toList $ responseHeaders response
        assertEqual "got back no replay path, as expected" Nothing mreplay
  ]

withServer :: (Client ClientM TestAPI' -> ClientEnv -> IO ()) -> IO ()
withServer body =
  testWithApplication (pure testApplication) $ \port -> do
    manager <- newManager defaultManagerSettings
    baseURL <- parseBaseUrl [i|localhost:#{port}|]
    let clientEnv = mkClientEnv manager baseURL
    let cl = client (Proxy @TestAPI')
    body cl clientEnv
