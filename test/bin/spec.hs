module Main ( main ) where

import Test.Tasty ( TestTree, defaultMain, testGroup, localOption )
import Test.Tasty.Runners ( NumThreads(..) )

import qualified HelloServantTest
import qualified ReplayTest
import qualified WithRawRequestTest
import qualified WithCookiesTest
import qualified ProvideCookiesTest
import qualified RequestSizeLimitTest

main :: IO ()
main = defaultMain spec

spec :: TestTree
spec = localOption (NumThreads 1) $ testGroup "servant-example-combinators"
  [ HelloServantTest.spec
  , ReplayTest.spec
  , WithRawRequestTest.spec
  , WithCookiesTest.spec
  , ProvideCookiesTest.spec
  , RequestSizeLimitTest.spec
  ]
