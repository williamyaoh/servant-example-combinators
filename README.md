# servant-example-combinators

Example Servant combinators that allow automating various server middleware
functions in a Servant-y way. Examples include:

* Automatically adding response headers
* Automatically parsing cookies, and providing those cookies to other combinators
* Checking incoming request size
* Accessing the raw request

See [the accompanying post](https://williamyaoh.com/posts/2023-02-28-writing-servant-combinators.html)
for details on how these work.
